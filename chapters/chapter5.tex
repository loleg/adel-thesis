% Chapter 5
\chapter{Toolchain}

This chapter is aimed at presenting the toolchain, used for the activity analysis in the thesis: from the data acquisition towards quality assessment of captured sensor data and activity visual analysis. In the chapter we give an overview of the features and of the user interface of the tool. The software used in this thesis can be found in the attached materials described in \autoref{app:_appendix_c}.

In \autoref{sec:mobile_application} we have already described the mobile application, which we used for sensor data acquisition. After the first database chunks are sent from \textit{sensorlogger} mobile application to the backend system, they can be encoded and merged in one \textit{sqlite} file. This can be done by executing the \textit{process\_data.py} script. The script uses a password written in the \textit{sensorlogger}'s configuration file in order to encode the data. Further, the features from the sensor data can be extracted and the output data can be stored in the visualization database. The visualization database is built using the \textit{MonetDB} \footnote{\textit{MonetDB:} \url{https://www.monetdb.org/}} column-oriented database management system, because it provides high-performance on multi-table join operations with multi-millions rows, which is important in sensor data fusion. The import into the visualization database can be done by executing \textit{importToMonetDB\_version.py} script, which takes as parameters the source merged \textit{sqlite} file with sensor data and target \textit{monetdb} database schema. 

The activity visual analysis tool consists of three main windows: the welcome window, the data quality assessment window and the activity analysis window. In the welcome window (see \autoref{fig:welcome_screen}) activity recordings are listed. The welcome window consists of three sections: test recordings, activity recordings and a form to add a new recording. By adding a new recording the analyst declares an activity or test recording. \textbf{From date} and \textbf{to date} are timestamps, that correspond to the start and end time of the activity recording and that are needed in order to query the visualization database.

\begin{figure}[h]
  \centerline{
    \includegraphics[width=1.2\textwidth]{welcome_screen}
    }
     \caption{Activity visualization tool: welcome window.}
     \label{fig:welcome_screen}
\end{figure} 

The data quality window contains statistics about the percentage of the recorded data amount in comparison to the expected data and the two-dimensional histograms (see \autoref{fig:hex_bin_5_seconds_test}), which are used in the assessment of the synchronicity properties of the recorded data. There will be explained more about the visualizations for data quality in \autoref{sec:comparison_data_quality_for_settings}.

The activity analysis window contains four sections, where the data analyst can \begin{inparaenum}[ 1\upshape)] \item classify the loaded contextual data, \item scan the assigned labels and produce a classification statistics, \item discover the action patterns and \item export the result of the activity analysis into files\end{inparaenum}. 

\begin{figure}[h]
  \centerline{
    \includegraphics[width=1.2\textwidth]{sunday_walk_highlighting}
    }
     \caption{\textit{Sunday walk} activity: visual data classification.}
     \label{fig:sunday_walk_highlighting}
\end{figure}

At the first step of the analysis process we make a classification of the data using the visualization (see, \autoref{fig:sunday_walk_highlighting}). The visualization consists of several parallel time series. The number of time series equals to the number of the atoms in the contextual information data samples. The points on the parallel time series, which are corresponding to the same contextual information data sample are associated with the same identification number. In \autoref{fig:sunday_walk_highlighting} for every identification number there are associated exactly three data points. The text fields and the sliders on the right side of the visualization are used to assign the labels to the data, i.e. classify it. Every text field corresponds to one class of data in the associated time series. Using the symbolic add button one can create a new label. The sliders are used to define the separating border of the classes. In order to label the data using \textit{n} labels there will be created \textit{n-1} sliders. Outermost classes of data are naturally bordered with two extremes: minimum and maximum values of the time series. The sliders may set any value, which is in range of the corresponding time series data. The classes of the data are encoded with color, i.e. to the data of the same class will be assigned the same color. While moving the sliders to the right or to the left a visual analyst may observe the coloring changes in real time. If the analyst wants to navigate into data insights, then the time series can be synchronously zoomed using the brush tool, which is under the main frame of each visualization. The zoom changes in one time series will be automatically reflected on other time series. This interaction technique is called \textit{link and brush}. In our particular visualization this technique enables simultaneous navigation to the insights in the time series data, while preserving the positional relationship of data points. The time series can be added to the synchronous zoom events using special switchers, thus the zooming performance can be sometimes optimized, by excluding the unneeded series. If a visual analyst hovers the pointer over a data point in one time series, then the data points of the corresponding data sample will be highlighted in other time series. There is a handy tooltip, which shows its timestamp, its value and the assigned label for any focused data point (see \autoref{fig:tooltip}).          

\begin{figure}[h]
  \centerline{
    \includegraphics[scale=0.8]{tooltip}
    }
     \caption{Tooltip}
     \label{fig:tooltip}
\end{figure}

In the sections 2 and 3 of the activity analysis window the labels assigned by an analyst can be scanned and further, action patterns can be discovered. After the scan of the assigned labels the statistical information about the recording can be produced. The histogram in \autoref{fig:sunday_walk_labels_distribution} shows the percentage distribution of the labeled contextual information packets in the recording and the table in \autoref{fig:table} shows mean, variance and quantity information for every assigned label. After that the analyst can adjust the sliding window size and compare the computed action patterns with ground truth annotations (see \autoref{fig:sunday_walk_sub_figures}, \autoref{fig:way_to_office_sub_figures}).

\begin{figure}[h]
  \centerline{
    \includegraphics[scale=0.8]{table}
    }
     \caption{Tooltip}
     \label{fig:table}
\end{figure}

The visualization provides a data export for discovered action patterns and for labeled contextual information. The export can be launched by a click on the corresponding buttons at the bottom of the window. The output format of the data is given in \autoref{app:_appendix_c}.  


 
