% Chapter 2
\chapter{Related Work}

\section{Activity recognition on mobile}
The research area of \textit{activity recognition} is historically related to the \textit{plan/goal} recognition problem \citep{Yang:2009:ARL:1661445.1661450}, \citep{choujaa2010activity}. One of the earliest formal descriptions of the plan recognition process has been given by H.A Kautz in \citep{Kautz:1987:FTP:37652}. The objective of plan/goal recognition is to recognize a plan or a higher-level goal of the agent, while observing fragmented sequences of its actions \citep[~p.1]{Kautz:1987:FTP:37652}. Most of the early activity recognition systems accepted symbolic representations of the agent's actions (raw sensor data became broadly accessible for interested community quite recently) and produced an output in form of symbolically represented high-level goals \citep{Yang:2009:ARL:1661445.1661450}.

Whereas the human activities are diverse and complex, a big part of the research work on mobile activity recognition consider some limited pre-defined sets of simple activities \citep{choujaa2010activity}. Sometimes, the contents of these sets can depend on the explicit use in the activity recognition process of some particular sensors of the smartphone. For example, J. R. Kwapisz et al. describe a system to recognize six activities - \textit{jogging, descending stairs, ascending stairs, sitting and standing} - from the cell phones accelerometer in \citep{ Kwapisz:2011:ARU:1964897.1964918}. In context of such a kind of motion-oriented activities, the activity recognition problem can be sometimes referred as \textit{motion-based} activity recognition. In \textit{location-based} activity recognition the human activities are approximated with location: at home, in the office etc. To detect the user's location cellular, Bluetooth and GPS signals are used. In order to recognize simple activities from the pre-defined sets statistical classification techniques, namely \textit{generative} and \textit{discriminative models}, are used in smartphone-based activity recognition systems \citep{choujaa2010activity}. The generative statistical techniques, like \textit{Bayesian network}, \textit{Hidden Markov Model}, \textit{Dynamic Bayesian Network}, model the data as joint probabilities distribution over features and the labels from the pre-defined activities set. In the discriminative models, like \textit{Support Vector Machine}, \textit{Decision Tree} or \textit{Artificial Neural Network}, the activity is classified based on the difference between features of input data.  

The human high-level activities may be characterized as complex and typically long term. Furthermore, similar high-level activities may be interpreted differently, and often it is difficult to establish, when the activity was started and when it was finished \citep{Kim:2010:HAR:1729467.1729487}. An example of high-level activity could be the \textit{way to the office}, which has all those characteristics. It is  complex, because it consists of multiple sub-activities, which we call \textit{actions}: \textit{ $a_1$:~walk}, \textit{$a_2$: take a tram}, etc. It can be of long duration, up to several hours. The same sequence of actions, corresponding to the \textit{way to the office}, may correspond to another activity(for example, \textit{way to the supermarket, which is near to the office}). And different sequences of actions may have the same goal (suppose, that one person has to take a tram in order to reach his office, whereas another person needs to walk and then take a bus). Due to such characteristics of the high-level activities recognition of them can be challenging, because it is difficult to build a high-level conceptual model of such activities.

E. Kim et al. discuss two complementary techniques, which can be applied to human activity studies, in \citep{Kim:2010:HAR:1729467.1729487}. The techniques are \textit{activity recognition} and \textit{activity pattern discovery}. In activity recognition a high-level conceptual model has to be built initially, then the model can be implemented in an appropriate computing environment. In activity pattern discovery unknown patterns will be found from the low-level sensor data without any activity model definition or assumption. Therefore in this technique the computing environment will be built initially, then the sensor data will be analyzed and activity patterns found.

Huynh et al. proposed a method to discover user activity patterns using topic models in \citep{Huynh:2008:DAP:1409635.1409638}. The underlying idea of the method is intuition, that in order to perform some specific activity, a person has to perform actions, which are specific to that activity. It is similar to writing an article: as a rule the writer uses words, which are specific to the topic of the article. The sensor data is labeled using supervised learning algorithms or in an unsupervised fashion using clustering algorithm. Then the higher-level activity patterns are represented as probability distributions of such labels.     

In this thesis we imagine a user's high-level activity as a sequence of actions, like on \autoref{fig:activity_definition2}. Then we can say, that an activity is represented with its plan or scenario, where every entry is a user's action. Under action we understand some routine, which the user has to perform in order to fulfill the activity. For example, in the \textit{way to the office} activity possible actions could be \textit{walk to the tram stop}, \textit{take a tram}, then \textit{walk from destination tram stop to office}. Every action of the user is accompanied by contextual information. This information changes as the user performs another action. If the user carries a smarthone, then those contextual information can be registered using multiple sensors, which are available in the smartphone.

\begin{figure}[h]
  \centering
    \includegraphics[width=0.8\textwidth]{activity_definition2}
     \caption{Activity, actions and contextual information}
     \label{fig:activity_definition2}
\end{figure}

Detection of the smartphone user's high-level action patterns requires information from multiple sensors of the smartphone, because the accompanying contextual information is many-sided: visual, audible, movements, environment etc. However, the smartphone can't register all contextual information, therefore we call multisensory contextual information, provided by the smartphone, \textit{available contextual information}, see \autoref{fig:activity_definition2}. The available contextual information will be provided by contextual information sources which are hardware and software sensors, computing environment of the smartphone and time.   

      
\section{Software framework, supporting the data acquisition on mobile}
Plenty of software products enabling the capture and supporting the storage of the smartphones' sensor data have been proposed and implemented in recent years. The implemented data acquisition software products have various underlying concepts and the corresponding projects are in different development phases. For this project we have selected and compared three software products.

\paragraph{\textit{ContextRobot} mobile application} ~\\
\textit{ContextRobot} is an application that captures the smartphones' sensor data, introduced by D. Bannach et al in \citep{Bannach:2010:ITC:1864431.1864434}. The application allows to capture the data, like acceleration, magnetic field sensor, near field bluetooth devices, gps coordinates, cell towers, WIFI access points and every tenth sample of sound. The sensor recordings are stored as comma-separated values files in the internal storage of the mobile device.

\paragraph{\textit{Funf} framework} ~\\
An extensible open-source framework, named \textit{Funf} \footnote{\textit{Funf:} \url{http://www.funf.org}}, enabling the capture of different kinds of contextual information (including traditional sensor data) on \textit{Android} driven smartphones, initially has been presented by N. Aharony et al. in \citep{Aharony:2011:SFI:2072697.2073099}. The \textit{Funf} framework includes server side tools and the smartphone-side Android library. During the work on this master thesis several changes in the \textit{Funf} architecture have been done by the developers. The most actual structure of the smartphone-side architecture of the framework is presented on the \autoref{fig:funf}.

\begin{figure}[h]
  \centering
    \includegraphics[width=0.8\textwidth]{funf}
     \caption{\textit{Funf} smartphone side architecture}
     \label{fig:funf}
\end{figure}

Every type of signal, captured by \textit{Funf} is encased in a Probe object. In the \textit{Funf} framework the data acquisition from traditional sensors like accelerometer, GPS, gyroscope, WI-FI and others is extended with data acquisition from sources, which can be understood as non-traditional or synthetic sensors, like sound features, running apps, calendar and so on. (This was a reason for developers to use the more general \textit{Probe} terminology instead of the \textit{SensorProbe} terminology \citep{Aharony:2011:SFI:2072697.2073099}.) Every probe has a common set of states and methods. Every probe can be managed through its set of configuration parameters. It gives a possibility to extend the \textit{Funf} library with new probes or improve already existing ones. Version 0.4 of the \textit{Funf} has 39 built-in probes.

The data acquisition parameters can be configured with a JSON configuration file. Basically, the configuration file represents an expected data stream to be captured. The four root fields of the configuration file - \textit{upload, archive, data} and \textit{update} - specify the pipeline. The pipeline performs the acquisition of the probes, defined in the \textit{data} field of the configuration file, and manages actions like storing the probes data to the internal database, encoding and moving the data from the internal database to the file system, uploading the data to a backend storage using URL, specified in the configuration file, and updating the configuration file. One of the main parameters, which specifies the scheduling of the probes, is the \textit{interval} parameter. The interval, given in the configuration file and equals \textit{t} , denotes, that some specific probe acquisition task will be repeated every \textit{t} seconds.

Basically, the probe objects in the \textit{Funf} framework can be classified in one of the groups:
\begin{itemize}
\item the probes from the contextual informations sources, whose output is a continuous data stream, like the output of some of the sensors of the smartphone. This type of probes has another important parameter, which is called \textit{duration}. Duration is written in the probes configuration file and describes how long some specific probe has to be captured every \textit{t} seconds, where \textit{t} is the interval of the probe's schedule.
\item the probes from the contextual information sources, which have a discrete output, like probe of the user's contacts list from the address book of the smartphone.
\end{itemize}

\textit{Funf in a Box} and \textit{Funf Journal}\footnote{\textit{Funf Journal}: \url{https://play.google.com/store/apps/details?id=edu.mit.media.funf.journal}} are Android applications, built on top of the \textit{Funf} library and provided by the developers of the \textit{Funf} project.   

\paragraph{\textit{CommonSense} platform} ~\\
This platform is a commercial system with open source, supporting sensor data acquisition \footnote{\textit{CommonSense}: \url{http://www.sense-os.nl/}}. It is an actively developing project, combining different tools to support the sensor data acquisition, to store the data, to visualize them and classify the sensor data. Although by now (November 2013) the \textit{CommonSense} has been developed into a powerful system, at the beginning of the master thesis in October 2012 the system has been implemented only partially and had a limited functionality.

\textit{Sense Platform}\footnote{\textit{Sense Platform}: \url{https://play.google.com/store/apps/details?id=nl.sense_os.app}} is an application for Android driven smartphones, which is built on top of the \textit{CommonSense} library and provided by the \textit{CommonSense} developers.

\paragraph{Data acquisition requirements for the project} ~\\
In \autoref{tab:frameworks_comparison} we have listed requirements, which we find important to be met by the data acquisition software in order to use it in this project. The table shows, if the above mentioned applications meet the requirements or not.

One of the practical difficulties in capturing the multi-sensory data for the high-level activities(which are mostly long-term) in real-life settings is the requirement of long time autonomous work of the smartphone. Therefore, it can be required to schedule the data acquisition. The transfer of the collected data from the mobile device and storage management on the device must happen automatically. The data collections must be stopped or resumed by the user, this prevents the generation of unnecessary data. Loading the different configuration parameters will help to optimize the data acquisition for the particular activity recording.  

\begin{table*} [ht]
\setlength\extrarowheight{3pt}
\center
\begin{tabular}{|m{3cm}|m{2.4cm}|m{2.4cm}|m{2.4cm}|m{2.6cm}|}
\hline 
\textbf{Requirement} & \textit{ContextRobot} & \textit{Funf Journal} & \textit{Funf in a Box} & \textit{Sense Platform} \\ 
\hline
data capture can be scheduled & No & Yes & Yes & Yes\\
\hline
source code and documentation is available, if needed & No & Yes & Yes & Yes\\
\hline
simple data transfer(upload) on the working machine & No & No & Yes,~via \textit{Dropbox CoreAPI} & Yes, on proprietary server\\
\hline
loading the configurations & No & Yes & No & No\\
\hline
start/stop data acquisition, controlled from app's UI & Yes & Yes & No & Yes\\
\hline
output data format & CSV file per sensor & sqlite schema & sqlite schema & Unknown\\   
\hline


\end{tabular}
\caption{\textit{ContextRobot}, \textit{Funf Journal}, \textit{Funf in a Box}, \textit{Sense Platform}: meeting the requirements of the project}
\label{tab:frameworks_comparison} 
\end{table*}

After requirements analysis and taking in account the advantages and disadvantages of the already implemented applications, we decided to implement our own data acquisition application, based on the \textit{Funf} library.


