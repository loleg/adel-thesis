% Chapter 3
\chapter{Data Acquisition}
\label{chap:data_acquisition}

\section{Approach}
The data acquisition is done in 4 steps:
\begin{enumerate}
\item The user performs some activity, while carrying the smartphone in one of his pockets, according to the recording scenario. The contextual information for the performed activity is captured using the mobile application, installed on the smartphone.
\item Every action in the activity, performed by the user, is annotated, using the hand-held dictaphone. Some events may be annotated optionally. The voice annotations will be used for ground-truth later in the work.
\item The features are extracted from the collected data and the data loaded into a visualization database.
\item The voice ground-truth annotations are transcribed and saved in format suitable for our visualization. 
\end{enumerate}

\section{Mobile device and sensors}

The mobile device chosen for the data acquisition is the Samsung Galaxy SIII I9300 smartphone. At the moment when the project started (October2012) the Samsung I9300 was the most modern smartphone, available on the market, equipped with fast 1.4 GHz quad-core CPU. Our device may store up to 16GB data in its internal storage. Samsung I9300 is driven by an Android 4.1.2 operating system and provided with a plenty of different sensors. The full list of the Samsung I9300 hardware sensors with some of their characteristics is given in \autoref{tab:hardware_sensors}

The LSM330DLC accelerometer and gyroscope are integrated in one microchip, which is called \textit{inertial measurement unit}. The LSM330DLC inertial measurement unit has 6 degrees of freedom: 3 for accelerometer and 3 for gyroscope.

In addition to the hardware sensors, listed in \autoref{tab:hardware_sensors}, there are a plenty of software sensors available. Some of the software sensors may have multiple implementations: provided by iNemo engine, provided by Android OS, etc.  Software sensors, or virtual sensors, are sensors, the values of which are computed based on the hardware sensors values. The full list of software sensors, provided by iNemo Engine and Android with some of their characteristics is given in the \autoref{tab:synthetic_sensors}.

The iNemo engine is a software library, which performs sensor data fusion. The data for the based on the high number of states Kalman filter sensor fusion algorithm are provided by the 9-axis sensor suite. The iNemo engine software combines the 3-axis accelerometer, the 3-axis gyroscope, as well as of the 3-axis magnetic field sensor data resulting the output for rotation vector, linear acceleration, gravity and orientation virtual sensors.

Similar software sensors are provided by Android OS. As it is shown in \autoref{tab:synthetic_sensors} some of these software sensors have a higher energy consumption, than their counterparts, provided by iNemo engine. The explanation is, that iNemo software can manage the sensors more selectively and precisely during the sensor fusion process, than higher level Android software does, which results in a smaller power consumption.     

\begin{table*} [ht]
\setlength\extrarowheight{3pt}
\begin{tabular}{|m{1.8cm}|m{2.0cm}|m{2.5cm}|m{2.0cm}|m{2.5cm}|m{1.5cm}|}

\hline 
Vendor & Sensor name & Measurement description & Measurement unit & Max range; resolution & Power, mA \\ 
\hline 
Asahi Kasei Microdevices & AK8975C 3-axis Magnetic field sensor & geomagnetic field strength & X(\si{\micro\tesla}), Y(\si{\micro\tesla}), Z(\si{\micro\tesla}) & 2000; 0.06 & 6.8 \\ 
\hline 
ST Microelectronics & LSM330DLC 3-axis accelerometer & acceleration force, applied to the device & X(\si{m/s^2}), Y(\si{m/s^2}), Z(\si{m/s^2}) & 19.6133; 0.009576807 & 0.23 \\
\cline{2-6} 
 & LSM330DLC Gyroscope sensor & rate of rotation & X(\si{rad/s}), Y(\si{rad/s}), Z(\si{rad/s}) & 8.726646; 0.00030543262 & 6.1 \\
\cline{2-6}
 & LPS331AP Pressure sensor & atmospheric pressure & (\si{\milli\bar}) & 1260.0; 0.00024414063 & 0.045 \\
\hline 
Capella & CM36651 Light Sensor & ambient light level & (\si{lux}) & 121240.0; 1.0 & 0.2 \\
\cline{2-6} 
 & CM36651 Proximity sensor & proximity of an object to the view of the screen & two values: "far": 8.0(\si{cm}), "near": 0.0(\si{cm}) & & 1.3 \\   
\hline
\end{tabular}
\caption{Samsung Galaxy S3 I9300 hardware sensors.}
\label{tab:hardware_sensors} 
\end{table*}

\begin{table*} [ht]
\setlength\extrarowheight{3pt}
\begin{tabular}{|m{1.8cm}|m{1.9cm}|m{3.2cm}|m{2.0cm}|m{2.3cm}|m{1.0cm}|}

\hline 
Vendor; Software engine name(if known) & Sensor Name & Measurement description & Measurement unit & Max range; resolution & Power, mA \\ 
\hline 
ST Microelectronics; iNemo Engine & Orientation Sensor & degrees of rotation & X(\si{degree}), Y(\si{degree}), Z(\si{degree}) & 360; 0.015625 & 7.8 \\ 
\cline{2-6}
& Gravity Sensor & acceleration, caused by the force of gravity & X(\si{m/s^2}), Y(\si{m/s^2}), Z(\si{m/s^2}) & 9.80665; 0.009576807 & 6.1 \\
\cline{2-6}
& Linear Acceleration Sensor & acceleration force, applied to the device, excluding gravity & X(\si{m/s^2}), Y(\si{m/s^2}), Z(\si{m/s^2}) & 8.726646; 0.009576807 & 6.1 \\ 
\cline{2-6}
& Rotation Vector Sensor & orientation of device by providing rotation vector & Unitless & 8.726646; 0.00030543262 & 6.1 \\
\hline
Google Inc.; Android & Rotation Vector Sensor & orientation of device by providing rotation vector & Unitless & 1.0; \num[exponent-product = \times]{5.96e-8} & 13.13 \\ 
\cline{2-6}
& Gravity Sensor & acceleration, caused by the force of gravity & X(\si{m/s^2}), Y(\si{m/s^2}), Z(\si{m/s^2}) & 19.6133; 0.009576807 & 13.13 \\
\cline{2-6}
& Linear Acceleration Sensor & acceleration force, applied to the device, excluding gravity  & X(\si{m/s^2}), Y(\si{m/s^2}), Z(\si{m/s^2}) & 19.6133; 0.009576807 & 13.13 \\ 
\cline{2-6}
& Corrected Gyroscope Sensor & rate of rotation, corrected for noise and drift errors & X(\si{rad/s}), Y(\si{rad/s}), Z(\si{rad/s}) & 8.726646; 0.00030543262 & 13.13 \\
\hline
Samsung Inc. & Orientation Sensor & degrees of rotation & X(\si{degree}), Y(\si{degree}), Z(\si{degree}) & 360; 0.00390625 & 13.13 \\  
\hline
\end{tabular}
\caption{Samsung Galaxy S3 I9300 software sensors.}
\label{tab:synthetic_sensors} 
\end{table*}

In addition to the sensors, listed in the Table 1 and Table 2, the Samsung I9300 is equipped with GPS,  two Knowles Acoustics SPU041x MEMS microphones, NFC detector and 2 cameras. 


\section{Mobile application for data acquisition}
\label{sec:mobile_application}

To capture the smartphones' sensor data, an Android application has been developed (\autoref{fig:sensor_logger}).

\begin{figure}[h]
  \centering
    \includegraphics[height = 7cm]{sensor_logger}
     \caption{Main activity of the \textit{SensorLogger} application.}
     \label{fig:sensor_logger}
\end{figure}

The application uses the 0.4 version library of the \textit{Funf} framework. 
The application has the following basic functionalities:
\begin{itemize}
\item loading configuration for data acquisition from file, 
\item suspending/resuming the data acquisition process, 
\item uploading the captured data to the working machine (backend system) using the \textit{Dropbox Sync API}\footnote{\url{https://www.dropbox.com/developers/sync/docs/android}}.
\end{itemize}

Based on the selected configuration file, the probe data will be automatically collected. The values, delivered by the sensor probes, will be stored in the application's internal \textit{sqlite} database file. The archive service periodically encodes and moves this internal database file to the archive folder, which is in the file system of the smartphone. Archiving makes sure, that the internal database will not be too large, whereas the database files are encoded to eliminate misuse of the data. The upload service copies the encoded database files to the cache of the \textit{dropbox} client. The database files will be placed into a folder, corresponding to the configuration file. If the smartphone has an INTERNET connection, then the \textit{dropbox} client will perform the synchronization of the encoded \textit{sqlite} files in the cache with the folder on the backend system. On the server side the \textit{sqlite} files, stored in the dropbox special \textit{Apps} folder, will be decoded and merged. After that, the data can be imported to the visualization database (See diagram in \autoref{sec:data_flow}).

Simultaneously with the sensor data recording, the user's voice annotations and background audio noise have been recorded. The recording was done using the hand-held dictaphone Zoom H1. By means of the special miniature lavalier microphone, mounted somewhere near the neck of the user, the audio annotations, dictated by the user, were captured. Every audio annotation consists of a timestamp, when the particular action was started with accuracy of one minute, and the action name. At the end of the action corresponding end timestamp has been annotated, if it was needed.
Every audio background noise recoding has been started simultaneously with the data acquisition start. Later, the audio tags were transcribed and translated into JSON file, which can be loaded into visualization. Some annotated actions like, for example, "take the bus" may need a more accurate timestamp, than the timestamp with minutes precision. In that case, a more exact timestamp (with accuracy of seconds) of an action's start and end was taken from the audio player's internal clock by interpreting the corresponding audio noise.

Our strategy for data acquisition was to capture the data from as many sensors as possible of the Samsung S3 smartphone, even though not all of the recorded data will be used in activity analysis later. From the 39 built-in probes, available in the version 0.4 of the \textit{Funf}, we picked out 8 for the activity data collection. We didn't collect the probes, which imply active use of the smartphone, like address book's contacts probe, applications probe and so on. The reason for that is, that in this limited research we had no possibility to collect data from active smartphone users. The probes, which we managed to collect are hardware sensor oriented probes.

The sensor probes, which we were able to capture are presented on the \autoref{tab:data_captured}.
\begin{table*} [ht]
\setlength\extrarowheight{3pt}
\center
\begin{tabular}{|c|m{10cm}|}

\hline 
Probe name & Description \\ 
\hline
Audio features probe & 12 values of MFCCS features, L1 Norm, L2 Norm, LinfNorm,  PSD \\
\hline
Linear acceleration probe & x,y,z of the linear acceleration vector \\
\hline
Location probe & altitude, longitude, latitude \\
\hline
Pressure sensor probe & atmospheric pressure \\
\hline
Light sensor probe & illuminance \\
\hline
Magnetic field sensor probe & x,y,z of the magnetic field sensor \\
\hline
Bluetooth probe & addresses of bluetooth devices in the near field \\
\hline
WI-FI probe & addresses of WI-FI devices in the near field \\
\hline

\end{tabular}
\caption{Captured data.}
\label{tab:data_captured} 
\end{table*}

Let's briefly describe all of the probes from the \autoref{tab:data_captured} and outline the contextual information, which can be derived from the probes. An example of output information format for every kind of probe we captured is given in \autoref{app:user_annotations_appendix_a}.  

\paragraph{Audio features probe} ~\\
In this probe the signal of the smartphone's internal microphone is used. The probe data contains different features such as 12 values of MFCCS(\textit{Mel-frequency cepstral coefficients}), L1 Norm, L2 Norm, Linf Norm and PSD(\textit{Power spectral density}), which are computed directly on the smartphone.

L1 Norm and L2 Norm are scaled to a unit vector and computed as $\frac{\sum_{i=0}^{N}{\lvert audioSample_{i} \rvert}}{N}$ and $\sqrt{\frac{\sum_{i=0}^{N}{audioSample_{i}^2}}{N}}$ accordingly, where \textit{N} is a number of recorded samples in the buffer. Linf Norm is computed as $\max{(\lvert audioSample_{i} \rvert)}$, $i\in[1;N]$. 

PSD is computed across frequency bands and indicates the power of the sound in these bands. The following frequency bands are set: (50-250)\si{\hertz}, (250-500)\si{\hertz}, (500-1000)\si{\hertz}, (1000-2000)\si{\hertz}.

\paragraph{Linear acceleration probe} ~\\
This probe captures values delivered by the linear acceleration software sensor. The output data of the probe contain measured linear accelerations of the smartphone in orthogonal directions apart from accelerations caused by gravity force. The linear acceleration of the device is obtained using sensor fusion and computed as $\overrightarrow{a_{linear}} = \overrightarrow{a_{device}}-\overrightarrow{g}$. Usually, the information derived from this probe data is used to detect everything related to the users body movements and postures.  
\paragraph{Location probe}  ~\\
The data (the main data are latitude and longitude) for this probe may be delivered either by GPS or by Network (Cell-ID or Wi-Fi based). The location by GPS replaces the best location estimate, obtained by the network. However, indoors, where the GPS signal is unavailable, the coarse location estimation by the network may be given. 
\paragraph{Pressure sensor probe}  ~\\
The pressure sensor probe collects the values, delivered by the pressure sensor. The data represent atmospheric pressure in the environment.
\paragraph{Light sensor probe}  ~\\
This probe captures the output of the light sensor probe.
\paragraph{Magnetic field sensor probe}  ~\\
The data for this probe is obtained from the magnetic field sensor.
\paragraph{Bluetooth probe}  ~\\
The probe captures the MAC addresses of Bluetooth devices in the near field. The major part of modern smartphones are equipped with Bluetooth modules, therefore this probe information can be used to identify other smartphone users and detect interactions with them.
\paragraph{WI-FI probe}  ~\\ 
The probe captures the MAC addresses of devices with Wi-Fi in the near field. Wi-Fi access points have unique MAC adresses and as a rule have fixed locations, therefore in urban environments, where there is a high density of access points, the probe information can be used to track the places visited by user. 

\section{Data flow: from probe data to contextual information}
\label{sec:data_flow}

The overview of data flow through our information system is represented in \autoref{fig:data_flow}.
The whole system consists of four modules: mobile application, \textit{Dropbox} service, backend system and visualization client. The mobile application collects the probe data and sends it to the backend system using the \textit{Dropbox} service. On the backend side of the system the probe data will be processed and stored in the visualization database. Processing of the probe data may include the extraction of relevant features. The information stored in the visualization database we call \textit{contextual information}. From one probe data series can be extracted various contextual information. 

We call \textit{contextual information sample} or \textit{contextual information packet} is all contextual information in the visualization database, associated with the same timestamp. Supposed], that there are $N$ contextual information series in the visualization database. Assume, $t$ is a timestamp value in the database and $s_j^t, 1 \leq j \leq N$ are the contextual information values, associated with the timestamp $t$. Then the $N$-tuple $(s_1^t, s_2^t, s_3 ^t, ..., s_N ^t)_i$ is a contextual information sample with index \textit{i}.     
\begin{figure}[h]
  \centering
    \includegraphics[width=0.8\textwidth]{data_flow}
     \caption{Data flow diagram}
     \label{fig:data_flow}
\end{figure}

In order to build the action pattern visualization we compose the contextual information samples with the following data:
\begin{itemize}
\item Linear acceleration standard deviation,
\item Pressure first derivative,
\item Audio noise level.
\end{itemize}
Let's briefly describe these data and what kind of information can be obtained from it.

\paragraph{Linear acceleration standard deviation} ~\\
This data is derived from the linear acceleration probe data using the formula \ref{eq:std_dev}.
\begin{flalign}
\label{eq:std_dev}
& \sigma_{lin. acc.} = \sqrt\frac{\sum_{i=0}^{N}{(M_i -\overline{M})^2}}{N} , \quad \text{where}  & \\
& \sigma_{lin. acc.} \quad \text{-- sample standard deviation of the linear acceleration} & \\
& M_i = \sqrt{x^2 + y^2 + z^2} \quad \text{ -- magnitude of the instantaneous value of linear acceleration,} & \\
& N \quad \text{-- number of linear acceleration measurements,} & \\
& \overline{M} = \frac{\sum_{i=0}^{N}{M_i}}{N} \quad \text{-- average magnitude of linear acceleration.}
\end{flalign}

We use this data to describe the smartphone user's movements. If the user is not moving, then the magnitude of the linear acceleration value will be approximately zero. However, the positive values of the linear acceleration magnitude can be caused by external factors (as for example, in the case, when the user is in an accelerating train). User body movements can be characterized as acceleration values, which are tending to change. According to the physical significance of the standard deviation, the data shows how the magnitudes of the linear acceleration are spread in the measurement. The more intensive the module of the linear acceleration vector changes, the bigger is the value of this feature.

\paragraph{Pressure first derivative} ~\\
This information is computed as a difference between the average values of two consecutive pressure sensor probe samples \ref{eq:first_derivative}. 
\begin{flalign}
\label{eq:first_derivative}
& dP = \sum_{i=0}^{N}{P_i^{(t)}} - \sum_{i=0}^{M}{P_i^{(t-1)}}, \quad \text{where} & \\
& dP \quad \text{-- pressure difference,} & \\
& P^{(t)}, P^{(t-1)} \quad \text{-- pressure samples, captured at time $t$ and $t-1$}  
\end{flalign}
We call this data series pressure first derivative, although we omit division on the time between two samples. Its done for the sake of simplicity, because we perform the data acquisition at regular intervals.

The data show, how the air pressure was changed since the last measurement. Since the air pressure and altitude are directly related to each other, the changes of the pressure values can be interpreted as changes of the user's altitude. This may give us a possibility to detect user actions, which are connected with an altitude change, like \textit{lift}, \textit{mountain funicular}, \textit{stairs} etc.
\paragraph{Audio noise level} ~\\
Audio noise level information is obtained from the audio features probe data. 

We use L1 Norm values in order to compute the audio noise level \ref{eq:audio_noise_level}. 
\begin{flalign}
\label{eq:audio_noise_level}
& A_{db}=10 * log_{10}{(L1Norm^2)} &
\end{flalign}
The audio noise level value will indicate the actual level of the environmental noise in \textit{dB}. Because we place the smartphone in the pocket during the data acquisition, we expect that noise produced by the user's clothes will be added to the environmental noise, too.


\section{Data quality issues}
In order to explore the potential data issues a visualization like on \autoref{fig:data_issues} has been implemented. The visualization aims to show temporal issues in the captured data. For each of the contextual information series we put the points, corresponding to the data timestamps on the timeline. The red repeated lines on the visualization mark off the expected timestamp $t^{expected}_n$ of the \textit{n}-th data sample, starting from the first sample in the series~ \footnote{The first data sample in the series is not presented on the \autoref{fig:data_issues}.}. The distance between two consecutive red lines is equal to the data acquisition \textit{interval}. 

Three key types of temporal issues, associated with the captured data quality have been observed on the visualization:
\begin{itemize}
\item the sensor probes, corresponding to the same packet are not aligned on time with each other,
\item absence of the sensor data in some packets,
\item delay of the whole contextual information packet from the time, when the packet was expected.
\end{itemize}

\autoref{fig:data_issues} illustrates these data quality problems. The dots are denoting the presence of the probe data for the timestamp \textit{t}. For some data there is more than one dot. This is due to the fact that the sensor probe may be represented by several features.

\begin{figure}[h]
  \centering
    \includegraphics[height = 9.0cm]{data_issues}
     \caption{Temporal issues of the data.}
     \label{fig:data_issues}
\end{figure}

There could be several reasons for such kind of data issues:
\begin{itemize}
\item Mobile app crash: there were periods of time, when the data has not been recorded~\footnote{One of reasons for crashes in the library, caused by the concurrent access to the \textit{sqlite} file has been founded during work on the thesis.}.
\item The sensor probe value can be absent, because the given time interval, when the probe object can deliver the message with a value to the \textit{Pipeline} object, has been exceeded.
\item Some sensors may need an additional initialization time, before the value can be read.
\item Sensors of the smartphone are asynchronous.
\end{itemize}

The activity visualization method, which we apply in the thesis (see \autoref{chap:visual_data_exploration_approach}), requires, that the above mentioned data issues will be minimized. Our hypothesis is, that the data quality issues appear, because we try to record too much data, therefore we may obtain a better quality, if we minimize the probes' data stream. This can be done by reducing the set of probes to be recorded and/or by tuning the probes capture frequency (\textit{interval} and \textit{duration} parameters). First, we reduced the set of probes in order to record as much different contextual information aspects as possible without redundancy. In particular, we did not include the \textit{AccelerometerSensorProbe}, but have chosen the \textit{LinearAccelerationProbe}(${linear\ acceleration} = acceleration - {acceleration\ due\ to\ gravity}$\footnote{\url{http://developer.android.com/guide/topics/sensors/sensors_motion.html}}), since both sensor probes are aimed to show the user's movements according to our concept, so there is no need to include both of them. 

Next, prior to user activity recording, we have done two series of settings tests. In the settings tests we try to capture the data of the selected set of probes for some fixed amount of time with various \textit{interval} and \textit{duration} settings for the probes. In the first series of tests the parameter, which we control is the \textit{duration} of the relevant probes and it equals to 1 sec, while the \textit{interval} parameter will vary over the tests. We measure the number of actually captured samples of data and compare it with the expected number of samples in the data series. In the second series of tests we try to tune the \textit{duration} parameter only of those probes, for which we obtained not enough data, while the \textit{interval} parameter stays fixed. The goal of the settings tests was to find a configuration setting for the implemented data acquisition mobile application, which will minimize the data quality issues, while maximizing the data stream to be captured. The result of the settings tests will be presented in \autoref{chap:use_cases}.

\section{Recording scenarios}

Our approach will be demonstrated with two activity recordings, which we named \textit{Sunday walk} and \textit{Way to office}. The activities are chosen to be long-term and to consist of multiple actions, i.e. complex activities. For both activity recordings we considered simple recording scenarios. A recording scenario is a \textit{plan of activity} or a \textit{list of actions}, which have to be done by the user in order to perform the activity. If the user's actual actions series is coinciding with that plan (list of actions), then one can say, that the user performs the activity, which is defined by that list of actions. In the \textit{Sunday walk} the user was asked to perform a walk up to the Gurten hill, which is near Bern, from his home, then take a cable car on the top of the hill and return to the home using the public transport. In the second scenario the user was asked to visit his office, departing from the home, using the public transport of the city. 

Simultaneously with the probe data acquisition, using the \textit{SensorLogger} mobile application, the user annotated his actions, as we already described in \autoref{sec:mobile_application}. The annotations were transcribed and imported into the visualization. Later we used them as a ground-truth reference in the action pattern analysis. The annotations provided by the user are given in the \autoref{app:user_annotations_appendix_b}.  
 
