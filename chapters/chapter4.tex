% Chapter 4
\chapter{Visual Data Exploration Approach}
\label{chap:visual_data_exploration_approach}

In order to build the visualizations we used the \textit{d3} \footnote{\textit{d3:} \url{http://d3js.org/}} JavaScript library in the front-end and \textit{flask} \footnote{\textit{flask:} \url{http://flask.pocoo.org/}} python framework in the back-end. \textit{D3} allows to create elements of the document based on the arbitrary data and apply data-driven transformations in the document.

After the user refers to the visualization server, the server returns the list with all available data recordings declarations to the user's browser. The user can add new data, or visualize already existing activity recordings.
The data recording declaration consists of:
\begin{itemize}
\item recording name, 
\item recording start time, 
\item end time, 
\item period of the sensor probes schedule,
\item duration of the sensor probe schedule,
\item SensorLogger configuration file name, 
\item user remarks and user annotations file name, if any.
\end{itemize}
Moreover, the recordings are classified into two groups: activity recordings and test recordings.
For the test recordings we show only data quality visualization, for activity recordings both action pattern and data quality visualization.
 

\section{Data visualization approach: data quality}
To ensure the quality of the recorded data several visualizations have been done. The visualizations are aimed to discover issues in the data and to produce a statistical information. The statistical information will be used to decide, if the corresponding data recording has sufficient quality in order to use the data in the action pattern visualization. In the thesis we focused on the evaluation of the time-dependent quality of the data.

The issues are pointed out in \autoref{chap:data_acquisition}: the contextual information atoms corresponding to the same packet are not aligned on time with each other; absence of the contextual information atoms in some of the packets or the whole packet is not present for expected timestamp; delay of the whole contextual information packet from the time, when the packet was expected. Of course, in one data recording all of the three issues can be present, but some of them are more important to decide, if the recording has enough quality to apply our action pattern visualization method. \autoref{tab:data_issues} represents the priority order of the data issues and explains the underlying rationale of assigning the priority. The higher is the number of the issue, the less important it is for the decision, if the data recording has enough data quality.

\begin{table*} [ht]
\setlength\extrarowheight{3pt}
\center
\begin{tabular}{|m{2cm}|m{4.5cm}|m{7cm}|}
\hline 
Priority (No.) & Data issue & Explanation \\ 
\hline
1 (the most important) & Absence of the contextual information atoms in the packet & If one or more atoms are missing in the packet, then the whole packet is not valid and can not be used in the action pattern visualization. \\
\hline
2 & The contextual information atoms, corresponding to the same packet are not aligned on time with each other. & This issue is related to the synchronization window size. The problem of this issue is that, if the synchronization window is big enough, then we can not claim, that the contextual information atoms in the packet are corresponding to the same action performed by the user. \\
\hline
3 & Delay of the whole contextual information packet from the time, when the packet was expected. & One of the goals of the data acquisition is that we expect to obtain the packets in regular time intervals. \\
\hline

\end{tabular}
\caption{Data issues.}
\label{tab:data_issues} 
\end{table*}

In order to load the data into the data quality visualization, we make a request to the database, which returns recording parameters (duration, interval, data stream name) and the timestamps of the data we are interested in: audio features probe, linear acceleration features, pressure features. The data needs to be assembled into packets, i.e. data fusion has to be done.

On the visualization side the timestamps will be assembled into packets by the use of a special algorithm. The algorithm first orders the timestamps ascending in stacks, then sequentially associates the timestamps to the packets, while popping the data from the stacks. If all stacks with timestamps are empty, then the algorithm terminates. For three data streams in our contextual information data the algorithm uses three stacks. In the first step the algorithm finds and stores the minimum timestamp $t_{min}$, which is on the top in one of the stacks. The timestamp $t_{min}$ is the practical timestamp of the current packet. The theoretical timestamp of the next packet equals $t_{min} + interval$. Then the timestamps, stored on the tops of the data stream stacks, will be assigned to the same packet, if they are less than the expected timestamp of the next packet. The difference between the biggest and the smallest timestamp in the packet we call \textit{synchronization window size}. The synchronization window size can be understood as some kind of multisensory jitter, analogously to a sampling jitter, which exists in multisensor data. The Synchronization window size is an objective measure of the second data issue. The smaller the packet's synchronization window size, the less significant is the second data issue for that packet.

Actually, the theoretically expected timestamp of the packet can be computed in two ways:
\begin{itemize}
\item based on the previous packet's timestamp: $ t^{expected}_n = t_{n-1} + interval $ 
\item based on the first packet's timestamp: $ t^{expected}_n = t_{1} + (n-1)*interval $. 
\end{itemize}

The expected timestamp of a packet is an objective measure of the third data issue. It has a similar nature to a packet jitter in computer networks, which consists in variability of the packet latency over time.

In order to evaluate the second and the third data issues we build 2-D histograms. The histograms are explained in \autoref{sec:comparison_data_quality_for_settings}. 

The first data issue can be evaluated as a certain percentage of the full(good) data packets, which contain all atoms (i.e. there is a value for every data stream in the packet), to the expected number of packets, where the expected number of packets is computed as $\lceil\frac{recordingDuration(seconds)}{interval(seconds)}\rceil$, and number of full packets is given by algorithm above. 

\section{Data visualization approach: action pattern}
\label{sec:activity_pattern}

Suppose, there are $ M $ contextual information packets and every contextual information packet $ p_{i}, 1 \leq i \leq M $ contains $ N $ contextual data values $ s_{j}, 1 \leq j \leq N $ (see \autoref{fig:activity_pattern_approach1}). All contextual information packets contain the same number of contextual data values. Each of the contextual information packets is defined with an unique timestamp and corresponding contextual data values.

\begin{figure}[h]
  \centering
    \includegraphics[height = 2.5cm]{activity_pattern_approach1}
     \caption{action pattern approach 1.}
     \label{fig:activity_pattern_approach1}
\end{figure}

Then the action pattern visualization will be done in three steps:
\begin{enumerate}
\item The user assigns text labels to all contextual data values in every contextual information packet using the visualization. In order to classify the contextual data values the user uses the annotations, which are presented with their visualization, as reference.

There are $ N $ disjoint sets of labels. We take in account, that the contextual data series in the recording are semantically distinguishable, because they are representing different aspects of the contextual information. Recall, the linear acceleration probe corresponds to the user's movements, but the audio level probe indicates the noisiness of the environment. Then the sets of labels, corresponding to the different contextual data are semantically distinguishable. Lets denote $ L_j, 1 \leq j \leq N $  is the set of labels corresponding to the $j$-th contextual data series. To every contextual data value can be assigned only one label from the corresponding set $ L_j $.

\item Scanning the labels from the visualization.

In the second step we obtain a set $ A $, which is the set of labels, assigned to the contextual information packets $ p_{i} $. Every element of $A$ is a text label, obtained by the concatenation of the labels, assigned to the contextual data values.

There are $ | A | \leq \prod_ {j=1}^{N} | L_j | $possible different label assignments for contextual information packets $p_{i}$.  In this step we can compute an overview statistics showing the probability to occur of the label $ a \in A $ in the activity recording.

\item For visualization of the action patterns we apply the sliding window approach.

Our objective in the third step is to compute the probability distribution of labels, assigned to contextual information packets $p_{i}, 1 \leq i \leq M$, over the recorded activity time. Doing that, we generate a series of sets of labels, obtained in step 2. The user choses the size of the sliding window, which is given in packets number. 
Suppose, that the user has set the size of the sliding window to $w$ (\autoref{fig:activity_pattern_approach2}).

\begin{figure}[h]
  \centering
    \includegraphics[height = 3.0cm]{activity_pattern_approach2}
     \caption{action pattern approach 2.}
     \label{fig:activity_pattern_approach2}
\end{figure}

Then the series of length $M - w + 1$ will be generated. 
In every window of the series we compute a probability for the occurrence of every distinct label from the set $A$, i.e. $\forall distinct a \in A, 1 \leq k \leq (M-w+1) $ we compute $p(a | w_k )$. Additionally we associate a timestamp to every window by taking an average of the contextual information packets, contained in the window.

This leads to the following output:
\begin{flalign*}
& w_1 : (timestamp_1 , \{ p(a_1 | w_1), p(a_2 | w_1), ..., p(a_{| A |} | w_1 ) \} ); \\
& w_2 : (timestamp_2 , \{ p(a_1 | w_2), p(a_2 | w_2), ..., p(a_{| A |} | w_2 ) \} ); \\
& {...} \\
& w_{M-w+1} : (timestamp_{M-w+1} , \{ p(a_1 | w_{M-w+1}), p(a_2 | w_{M-w+1}), ..., p(a_{| A |} | w_{M-w+1} ) \} );
\end{flalign*}
 
\end{enumerate}

Finally rearranging the items, for every distinct label $a \in A $ we put a line on the chart with timestamps ordered over the x-axis and probabilities over the y-axis.


