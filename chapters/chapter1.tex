% Chapter 1
\chapter{Introduction}

\section{Motivation}
Humans have biological sensors: special cells that are sensitive to light, motion, sound, humidity, temperature, and other stimuli. We use the information delivered by such sensors in cognitive processes as contextual information. The raw or absolute signal values of the senses are not essential for higher-order processes in the brain - they are interpreted. For example, the specific signal values of temperature-sensitive cells are not in themselves significant for decision making, but it is more important to know if it is cold or hot in the situation relative to prior experience. Combining inputs from multiple biological sensors and applying prior knowledge, a person may recognize the situation to make high-level conclusions.

The main purpose of technical devices is to free us from certain tasks, perform them in order to facilitate everyday life, and hopefully to let people work more on tasks which require creative thinking. Smartphones are portable digital communication devices, the use of which is growing rapidly every year. Most modern smartphones are not only a combination of telephone and computer, they are also equipped with a diversity of sensors, which are capable of measuring some physical quantity and converting it into a signal. 

Sensors such as the accelerometer, gyroscope, light sensor, magnet field sensor, microphone, localization sensors, temperature and humidity sensors can be found in many modern devices. Because of their mobility, sensing possibilities and computational power, smartphones may be very effective in combination with context-dependent services such as health monitoring, logistics optimizations, augmented memory, lifelogging, social networking, or environmental monitoring. In services such as augmented memory or lifelogging, it may be necessary to detect the high-level activities of the user who carries the smartphone. 

In applications like smart planners, it may be necessary to detect the actions of the user in order to measure state, and output suggestions for the next steps of a given plan. Activity detection could be done by making use of the multisensory data of the smartphone. There are several practical challenges associated with high-level activity detection on the smartphone: large multisensor data streams need to be stored and processed; the sensors of the smartphone are asynchronous; activity detection requires long term multisensory data acquisition, and therefore the battery life of the smartphone can be an issue. There is even uncertainties associated with the term activity itself, due to a lack of strong formal definitions and standards.

\section{Thesis goal}
The main goal of the thesis is to investigate if it is possible to detect high level activities from raw sensor data without modeling the activities. In order to explore the problem we apply information visualization techniques. There is an supposition, that as a user performs an activity following some plan, this coincides with a sequence of actions needed in order to fulfill the activity. Therefore we imagine the users activity as a sequence of actions and try to discover the patterns of these actions.

In order to reach the main goal of the thesis, the following steps are done:
\begin{enumerate}
\item Collect sensor data from a smartphone while the user performs some particular activity. The data acquisition needs to be annotated by the user, in order to be used as the baseline in action pattern analysis.
\item Make assertions about the quality of the collected sensor data.
\item Visualize the data.
\item Analyze the visualization, detect high-level action patterns.
\item Compare the detected patterns with baseline annotations.
\end{enumerate}

We demonstrate the approach with two kinds of baseline annotations:\begin{inparaenum}[\itshape a\upshape)] \item human-oriented action annotations and \item system-oriented action annotations \end{inparaenum}. In the former case the baseline labels are given from the perspective of the user. For example, \textit{take a tram} action may include \textit{wait for the tram}, which was not necessarily annotated. In the latter case the actions were provided taking in account the system. For example, \textit{take a tram} was annotated exactly in the moment when the user entered the tram. 

\section{Structure of report}
The next chapter covers some background related to the main topic of the thesis, the most important definitions and our understanding of the concepts of activity and action will be presented. Some aspects of activity recognition on a mobile device will be discussed, and examples given of data acquisition frameworks. In Chapter 3 will discuss the process of data acquisition and practical problems associated with it. The following two chapters will explain the visual analytics methodology and present our toolchain. In Chapter 6 we provide some examples of action pattern exploration in the collected data using the visual analytics method. Chapter 7 continues with a discussion. The thesis ends with a chapter dedicated to a conclusion and further propositions.
