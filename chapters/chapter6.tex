% Chapter 6
\chapter{Use Cases}
\label{chap:use_cases}

\section{Collected data}

The whole data collection history can be divided into two parts: before the version 0.4 release of \textit{Funf} and after. A big part of data ($\approx$ 3 weeks) was captured with a mobile application, built on top of the pre-0.4 version of the \textit{Funf}, a few activity recordings were even annotated with ground truth. However, the quality of the recordings was not sufficient: probes data were not synchronized with each other and there was loss of data, caused by crashes in the probe services. In April 2013 \textit{Funf} version 0.4 has been released. The new version introduced some key changes in the architecture of the library, such as running the probes in one service instead of in a service per probe, re-design of the data output format and performing internal objects communication via function calls instead of less efficient communication via broadcasts and intents.

With the mobile application for smartphone, based on the version 0.4 of \textit{Funf} we have managed to collect the multi-sensory data for 21hrs:20mins of different activities and annotate them with ground truth. For two activity recordings \textit{Sunday walk} and \textit{Way to office} with a total duration of 2hrs:20mins we have transcribed the audio ground truth annotations and present the results of the application of the action pattern detection method, described in \autoref{sec:activity_pattern}.  

The ground truth annotations are obtained from the audio files, which have been recorded simultaneously with the data acquisition during the activity recording, as we already described in \autoref{sec:mobile_application}. The ground truth voice annotations were based on the following principles: 1) the user annotated mostly actions, i.e. something performed routinely, like for example \textit{walking on the street} or \textit{waiting}. These actions, which are defined by the recording scenario, were important steps in order to perform the target activity. 2) small events, like posture changes or environment events, were annotated optionally, if the user found them important for the activity. For the whole protocol of voice ground truth annotations for \textit{Sunday walk} and \textit{Way to office} activities see the corresponding tables in \autoref{app:user_annotations_appendix_b}.

\section{Comparison of the data quality for different acquisition settings}
\label{sec:comparison_data_quality_for_settings}
In order to find the optimal probe data acquisition settings (\textit{interval} and \textit{duration}) for the mobile application, presented in \autoref{sec:mobile_application}, we performed two series of tests. On the one hand, the optimal data acquisition setting aims at performing the probes recording with good quality (according to the criteria 1-3 on the \autoref{tab:data_issues}). On the other hand, the setting must provide a stream of good density for the capture of the probes, i.e. enough to detect the user actions using the approach from \autoref{sec:activity_pattern}. During one action, which may have a duration from a few minutes to tens of minutes, we may need to capture more than one probe packet. For example, if we will capture the probes every 5 seconds, then for an action, which has a duration of one minute, we will have 12 probe packets.   

In the tests we considered to capture and evaluate the quality of 30 minutes of the probe data. We evaluate the quality only of the probes, which we will use in the action pattern visualization: AudioFeaturesProbe, LinearAccelerationFeatures (represents LinearAccelerationProbe), PressureFeatures (represents PressureProbe). 

As we mentioned in \autoref{tab:data_issues}, the absence of one or more atoms in the contextual information packet makes the whole packet unusable in the action pattern visualization method. Therefore, it is important to know, how complete the contextual information in the activity recording is. In the first series of tests we keep fixed the \textit{duration} parameter of the probes, while changing the \textit{interval} parameter. The following settings have been chosen for the \textit{interval} parameter of every probe: 2, 3, 4, and 5 seconds. The \textit{duration} parameter has been set to 1 second for all probes. We measure the number of the captured samples and compare the value with the expected number of samples for every probe. In the next series of tests we fix the \textit{interval} parameter and try to tune the \textit{duration} parameter of some particular probes in order to reduce the acquisition fails of the probes.  

The results of the tests are presented in \autoref{tab:data_quality_first}. It is important to notice, that in an ideal case the number of expected probe samples for every probe equals to the number of contextual information packets, which we expect to have in these 30-minute test recordings. The last column of the table shows the percentage of the recorded useful data packets from the expected number of packets. The useful data fraction in the recording is computed as $Recorded = \frac{Number~of~good~packets}{Expected~number~of~ packets}$. From the table one can see, that with the $interval > 2sec.$ and $duration = 1sec.$ settings we were able to capture approximately the expected amount of data for the \textit{LinearAccelerationProbe} and for the \textit{PressureSensorProbe} in almost all tests. But we had not enough samples of the \textit{AudioFeaturesProbe} and therefore a low percentage of useful information in the recordings (see, tests \textnumero 1-4). In the tests \textnumero 5-6 we fixed the \textit{interval} parameter to 5 \si{sec} and increased the \textit{duration} parameter of the \textit{AudioFeaturesProbe}. (Recall, that the $interval \gg duration$). Finally, with the settings, which we used in the test \textnumero 6 we were able to capture the most complete data. The tests \textnumero 7-8 confirm the adequacy of the \textit{durations} for bigger \textit{intervals}.       

\begin{table*} [ht]
\setlength\extrarowheight{3pt}
\center
\begin{tabular}{|m{0.7cm}|m{1.2cm}|m{2.5cm}|m{1.5cm}|m{1.4cm}|m{1.5cm}|m{1.4cm}|m{1.3cm}|}
\hline 
Test \textnumero & Interval of the data capture, \si{sec} & Data name & Duration, \si{sec} & Number of samples & Expected number of packets & Number of recorded useful packets & Useful information, \% \\ 
\hline
1 & 2 & \scriptsize{AudioFeaturesProbe} & 1 & 0 & 900 & 0 & 0 \\
\cline{3-4}
  &   & \scriptsize{Lin.Acc.Features} & 1 & 340 &  &  &  \\
\cline{3-4}
  &   & \scriptsize{PressureFeatures} & 1 & 345 &  &  &  \\
\hline

2 & 3 & \scriptsize{AudioFeaturesProbe} & 1 & 1 & 600 & 1 & 0.17 \\
\cline{3-4}
  &   & \scriptsize{Lin.Acc.Features} & 1 & 600 &  &  &  \\
\cline{3-4}
  &   & \scriptsize{PressureFeatures} & 1 & 600 &  &  &  \\
\hline

3 & 4 & \scriptsize{AudioFeaturesProbe} & 1 & 0 & 450 & 0 & 0 \\
\cline{3-4}
  &   & \scriptsize{Lin.Acc.Features} & 1 & 450 &  &  &  \\
\cline{3-4}
  &   & \scriptsize{PressureFeatures} & 1 & 447 &  &  &  \\
\hline

4 & 5 & \scriptsize{AudioFeaturesProbe} & 1 & 0 & 360 & 0 & 0 \\
\cline{3-4}
  &   & \scriptsize{Lin.Acc.Features} & 1 & 360 &  &  &  \\
\cline{3-4}
  &   & \scriptsize{PressureFeatures} & 1 & 358 &  &  &  \\
\hline

5 & 5 & \scriptsize{AudioFeaturesProbe} & 2 & 88 & 360 & 88 & 24.44 \\
\cline{3-4}
  &   & \scriptsize{Lin.Acc.Features} & 1 & 360 &  &  &  \\
\cline{3-4}
  &   & \scriptsize{PressureFeatures} & 1 & 359 &  &  &  \\
\hline

6 & 5 & \scriptsize{AudioFeaturesProbe} & 3 & 358 & 360 & 358 & 99.44 \\
\cline{3-4}
  &   & \scriptsize{Lin.Acc.Features} & 1 & 360 &  &  &  \\
\cline{3-4}
  &   & \scriptsize{PressureFeatures} & 1 & 359 &  &  &  \\
\hline

7 & 10 & \scriptsize{AudioFeaturesProbe} & 3 & 180 & 180 & 178 & 98.99 \\
\cline{3-4}
  &   & \scriptsize{Lin.Acc.Features} & 1 & 180 &  &  &  \\
\cline{3-4}
  &   & \scriptsize{PressureFeatures} & 1 & 178 &  &  &  \\
\hline

8 & 30 & \scriptsize{AudioFeaturesProbe} & 3 & 59 & 60 & 58 & 96.67 \\
\cline{3-4}
  &   & \scriptsize{Lin.Acc.Features}& 1 & 60 &  &  &  \\
\cline{3-4}
  &   & \scriptsize{PressureFeatures}& 1 & 60 &  &  &  \\
\hline

\end{tabular}
\caption{Settings test: completeness of the data.}
\label{tab:data_quality_first} 
\end{table*}

\begin{figure}
\centering
\subfigure[Based on the first data sample in the series]
{\label{fig:hex_bin_5_seconds_test:a}\includegraphics[width=70mm]{test_5_seconds_absolute}}
\subfigure[Based on the previous data sample in the series]
{\label{fig:hex_bin_5_seconds_test:b}\includegraphics[width=70mm]{test_5_seconds_relative}}
\caption{Synchronicity properties of the data samples in test with data acquisition interval set to 5 seconds.}
\label{fig:hex_bin_5_seconds_test}
\end{figure}

Synchronicity properties of the data samples, captured in test \textnumero 6,  are represented using bivariate histograms, given on \autoref{fig:hex_bin_5_seconds_test}. The bivariate histograms are built using the hexagonal binning visualization technique \footnote{\textit{Hexbins:} \url{http://indiemaps.com/blog/2011/10/hexbins/}}. In general the binning technique is used to group $N$ values into less than $N$ groups (bins). The histograms show the frequency of occurrence of the data samples with given time-specific parameters (\textit{sync.window.size} and \textit{delay}) in the recording. The frequency is encoded with color on the histograms: the darker the color of the hexagon, the more data samples in the recording have parameters, which are limited with the borders of that hexagon. 

The \textit{synchronization window size} (y-axis) reflects the difference in time between the acquisition of the earliest contextual information atom in the data sample and the latest one. In the ideal case this indicator converges to zero, which means, that the data from the multiple sensors have been acquired synchronously and therefore contextual information atoms in some particular data sample are corresponding to the same event. From the histograms in \autoref{fig:hex_bin_5_seconds_test} we see, that the major part of the data samples recorded for test \textnumero 6, have the synchronization window parameter up to one second, which in context of the long term activity recordings may be accepted as a reasonable value. But we have to presume, that the probability, that the actual user's action can be changed during the acquisition of one data sample, is very low.   

The \textit{delay} (x-axis) shows, to what extent the data sample is delayed from the expected acquisition moment, i.e \textit{delay} of the n-th data sample can be computed as $delay_{n}=t_{n}^{planned}-t_{n}$. The delay can have a positive or a negative value, depending on when the actual data sample was acquired: later or earlier from the planned time. In the best case the \textit{delay} is expected to be zero, which means, that the data samples in the recording have been acquired evenly over the recording time. For each data sample the \textit{delay} indicator was computed in two ways: based on the first data sample and based on the previous data sample in the series. The histogram on Figure \autoref{fig:hex_bin_5_seconds_test:a} corresponds to the former way of delay computation and shows if the recording meets the stronger synchronicity claim: "the data sample has to be acquired without delay from the planned moment, regarding the recording start timestamp, i.e. at $ t^{planned}_n = t_{1} + (n-1)*interval $". The second histogram on Figure \autoref{fig:hex_bin_5_seconds_test:b} is related to the latter way of delay computation and shows if the recording meets the weaker synchronicity claim:"the next data sample has to be acquired without delay from the planned moment, regarding the previous data sample timestamp, i.e. at $t_{n} ^{planned} = t_{n-1}+interval$". Comparing the two histograms, we can assume, that our system can't support the stronger claim, because as we see in Figure \autoref{fig:hex_bin_5_seconds_test:a} a part of the data samples are shifted with $\approx 1.5\si{sec}$ delay. However, we can assert, that with high probability(99.44 \%) the system will capture the next sample in $(interval \pm 0.5)\si{seconds}$. 

\section{Action pattern visual analytics}

In this section the results of the high level action pattern discovery for two activity recordings (\textit{sunday walk} and \textit{way to office}) will be presented. Further the automatically detected pattern will be compared with the user's ground truth annotations. 

\paragraph{\textit{Sunday walk} activity} ~\\

We used the following labels to label the \textit{Sunday walk} activity recording (see \autoref{fig:sunday_walk_highlighting}):
\begin{itemize}
\item linear acceleration standard deviation - low movements, medium movements, high movements;
\item pressure first derivative - low change, stairs, lift;
\item audio noise level - silent, medium, loud.
\end{itemize}

The labels for the linear acceleration standard deviation are chosen according to the physical meaning of the data. We intentionally used a kind of abstract labels and not higher interpreation, like walking or running. The underlying rationale, which explains the choice of such kind of labels, is described in \autoref{chap:discussion}.

Once the data has been labeled, the labels can be read in order to be used in the next step of the action pattern detection. At the same time statistical information about data labeling can be produced. The \textit{Sunday Walk} recording contains $M=1342$ contextual information samples. The ranges of values for the classes and the results of classification are given in \autoref{tab:sunday_walk_statistics}.

Some observations on the \autoref{fig:sunday_walk_highlighting} and \autoref{tab:sunday_walk_statistics}:
\begin{itemize}
\item The pressure sensor is responsive enough to detect already small altitude changes. Recall that we recorded the data with intervals of 5 sec.  
\item Negative mean value in the \textit{low change} class of the pressure first derivative probe. This can be explained by the fact, that the user walked up to the hill, so the air pressure was decreasing. Thus, the sign can be interpreted as the vertical direction of the users movement, such as "-" means up and "+" down. 
\item Big variance of the audio noise level from one side corresponds to the nature of audio levels in real environments and how the audio levels are perceived by humans, but at the same time may signalize the need to add a new class to the existing set of label classes.
\item There is a correspondence between the pressure first derivative($\Delta P$) values given in \si{mbar} and the vertical speed. The speed can be represented as $c*\Delta P/interval$, where $c$ is some cofficient, depending on the actual altitude and weather conditions, $interval$ means the data acquisition interval, which equals 5 seconds in our activity recording. In case of small altitude changes $c$ can be approximated as $c=-1/(\rho g)$, where $\rho$ is the air density and $g$ the acceleration of gravity.     
\end{itemize}


\begin{table*} [ht]
\setlength\extrarowheight{3pt}
\center
\begin{tabular}{|m{2.0cm}|m{2.0cm}|m{2.0cm}|m{2.0cm}|m{2.0cm}|m{2.0cm}|}
\hline
Data Name & Label & Border values & Occurence, \textit{N} & Mean, \textit{avg} & Variance, \textit{v} \\
\hline
\multirow{3}{2cm}{Linear acceleration standard deviation} & Low movements & \textless 0.9 & 344 & 0.28 & 0.05 \\
                               							\cline{2-6}
                               							& Medium movements & 0.9 - 4.40 & 980 & 2.88 & 0.60 \\
                               							\cline{2-6}
                               							& Hight Movements & \textgreater 4.4 & 18 & 4.93 & 0.24 \\
\hline
\multirow{3}{2cm}{Pressure first derivative} & Low change & \textless 0.2 & 1299 & -0.02 & 0.01 \\
                               				 \cline{2-6}
                               				 & Stairs & 0.2 - 0.6 & 17 & 0.32 & 0.01 \\
                               				 \cline{2-6}
                               				 & Lift & \textgreater 0.6 & 26 & 0.99 & 0.08 \\
\hline
\multirow{3}{2cm}{Audio Noise Level} & Silent &  \textless 41.4 & 72 & 34.79 & 14.12 \\
                               		 \cline{2-6}
                               		 & Medium & 41.4 - 60.1 & 1158 & 54.11 & 12.08 \\
                               		 \cline{2-6}
                               		 & Loud & \textgreater 60.1 & 112 & 64.96 & 18.03 \\
\hline

\end{tabular}
\caption{\textit{Sunday walk}: the assigned labels statistics.}
\label{tab:sunday_walk_statistics} 
\end{table*}

The labels co-occurrences in the contextual information samples of the whole activity recording are represented on the histogram \autoref{fig:sunday_walk_labels_distribution}. There are 14 different combinations of the labels in \textit{sunday walk} activity recording. The most popular label combinations are (\textit{medium movements}, \textit{low pressure change}, \textit{medium noise level}) and (\textit{low movements}, \textit{low pressure change}, \textit{medium noise level}). In context of the \textit{Sunday walk} activity such combinations are expected to be more frequent, because the main part of the time the user spent on walking or staying in medium-noisy environments.   

\begin{figure}[h]
  \centerline{
    \includegraphics[width = 1.2\textwidth]{sunday_walk_labels_distribution}
    }
     \caption{\textit{Sunday walk} activity: labels co-occurrences in the contextual information samples.}
     \label{fig:sunday_walk_labels_distribution}
     
\end{figure}


\autoref{fig:sunday_walk_sub_figures} shows the probabilities of different possible labelings for contextual information samples over the activity time. The four action patterns, which are computed with four sliding window settings (95, 245, 995 and 2495 seconds), are annotated with ground truth action labels (represented by red rectangles), which are given by the user (for full user ground truth annotations see \autoref{app:user_annotations_appendix_b}). The size of the sliding window has an impact on the granularity of the discovered action pattern: the smaller the sliding window set, the more details can be observed in the action pattern.  

First of all we see, that there is a correspondence between the discovered action patterns and the ground truth action annotations. The correspondence between the annotated and the obtained action patterns is especially noticeable if the sliding window is set to a smaller value, than the duration of the corresponding action. In particular, the action \textit{visiting festival} lasts 15 minutes (see \autoref{app:user_annotations_appendix_b}) and for the action pattern visualizations on \autoref{fig:sunday_walk_sub_figures} a) and b) with the sliding windows set to 95 and 245 seconds accordingly there is a correlation with the ground truth annotations. But for the same action the correspondence is fuzzy in visualizations c) and d) with sliding windows set to 995 and 2495 seconds.

The label combination (\textit{medium movements}, \textit{low pressure change}, \textit{medium noise level}) is activated if the user performs the \textit{walking outside} action. However, this combination appears in other annotated by the user actions too. In particular, in the action \textit{take the bus N29}. This hints to the fact, that some actions may have hierarchical structure and may include other actions. 

Activations of the label combinations (\textit{medium movements}, \textit{lift}, \textit{medium}) and (\textit{low movements}, \textit{lift}, \textit{loud}) are corresponding to the action cable car ride. But the action \textit{take the funiculare "Gurtenbahn"} includes the activation of the combination (\textit{low movements}, \textit{low change}, \textit{medium}), which is more about waiting for the cable car and not riding in it. Thus, the ground-truth annotation of the user tells rather about the user's goal in the action, while the computed visualization discovers the user's actions by indicating changes in the contextual information. However, the sequence of the ground-truth annotations defines the \textit{sunday walk} activity, i.e. the general goal in the activity.        

\begin{figure}[h]
  \centerline{
    \includegraphics{sunday_walk_sub_figures}
    }
     \caption{\textit{Sunday walk} activity: distribution of the labels in contextual information samples over the activity time.}
     \label{fig:sunday_walk_sub_figures}
     
\end{figure}

The visualization in \autoref{fig:way_to_office_sub_figures} shows the action pattern discovery results for the \textit{way to office} activity. Here the walking actions alternate with transportation actions (see discovered label combinations (\textit{medium movements}, \textit{low change}, \textit{medium}) and (\textit{low movements}, \textit{low change}, \textit{medium})). In this recording the ground truth labels are given more precisely according to changes in the user's actions and according to expectations, how these changes will affect the contextual information. So that, for example, the \textit{take a tram} action was annotated exactly as the user entered the tram. Therefore we see, that the local maximums of the action patterns curves coincide with the starts of ground-truth actions.     

\begin{figure}[h]
  \centerline{
    \includegraphics{way_to_office_sub_figures}
    }
     \caption{\textit{Way to office} activity: distribution of the labels in contextual information samples over the activity time}
     \label{fig:way_to_office_sub_figures}
     
\end{figure}
